# Conclusion

```{r, include = FALSE}
source("setup.R")

df <- get_consolidated_data(output.path)
l_ff_calib <- readRDS(file.path(cache.path, "CalibrationFreeFlow.RDS"))
```


## Équation du seuil triangulaire tronqué dénoyé

La formule retenue est celle proposée par @bosDischargeMeasurementStructures1989
pour le seuil triangulaire tronqué à crête épaisse (Voir paragraphe \@ref(ch-loi-Bos-epais)).

L'équation proposée par @bosDischargeMeasurementStructures1989 est modifiée pour
assurer la continuité de l'équation entre la partie triangulaire et la partie rectangulaire (cf. Paragraphe \@ref(ch:transition), le débit $Q_1$ du seuil en
régime dénoyé devient&nbsp;:

\begin{equation}
  Q_1 = (1 - a) Q_T + a\ `r round(l_ff_calib$params$k, 4)`\ Q_R
  (\#eq:Qtransition-num)
\end{equation}

Avec $a$, la proportion du débit calculé avec la loi de seuil rectangulaire :

\begin{equation}
  a = \max(0, \min(1, \dfrac{h_1 / H_B - `r round(l_ff_calib$params$kht1, 4)`}{`r round(l_ff_calib$params$kht2 - l_ff_calib$params$kht1, 5)`}))
  (\#eq:Qa-num)
\end{equation}

Avec $Q_T$ la loi de débit du seuil triangulaire :

\begin{equation}
  Q_T = `r round(l_ff_calib$params$C1, 4)`\ \tan \dfrac{\theta}{2}\ h_1^{`r round(l_ff_calib$params[["triangular exp."]], 4)`}
  (\#eq:QT-num)
\end{equation}

Et $Q_T$ la loi de débit pour la partie rectangulaire du seuil tronqué :

\begin{equation}
  Q_{R} = `r round(sqrt(2) * l_ff_calib$params$C1, 4)` B_c\ \left ( h_1 - \dfrac{H_B}{2}  \right )^{`r round(l_ff_calib$params[["rectangular exp."]], 4)`}
  (\#eq:QR-num)
\end{equation}

Les erreurs sont en moyenne inférieures à 2 % pour les deux types de seuils
testés.
Les performances sont dégradées pour les faibles hauteurs d'eau avec un
écoulement ne se déroulant que sur la partie triangulaire.
Les erreurs restent cependant inférieures à 5 % pour le seuil D90
et des sous-estimations pouvant atteindre 10 % pour le seuil D135.
Pour ce dernier, la zone de hauteur concernée reste cependant très réduite,
la hauteur $H_B$ du début de la partie rectangulaire étant beaucoup plus faible
que sur le seuil D90.

## Formule en régime noyé

Le débit en régime noyé s'obtient en multipliant le débit $Q_1$ du régimé dénoyé
obtenu à l'Équation&nbsp;\@ref(eq:Qtransition-num) avec un coefficient de réduction de
débit selon l'équation suivante :

\begin{equation}
  Q = Q_1 \min\left(1, \left [ 1 - \dfrac{Q_2}{Q_1} \right ]^{`r round(kV, 4)`} \times `r round(kI, 4)`\right)
  (\#eq:villemonte-Qgen-modif-num2)
\end{equation}

L'erreur constatée est inférieure à 5 % pour l'ensemble des ennoiements à
l'exception des ennoiements supérieurs à 90 % où l'erreur avoisine les 10 %.

Ces résultats en font une équation valide pour une utilisation dans les
passes adoptant cette géométrie de seuil et sera implémentée prochainement
dans Cassiopée.

## Perspectives

Les travaux de @vennard1943submergence montre que le rapport de la hauteur d'eau
amont sur la hauteur de pelle a un effet non négligeable notamment pour modéliser
l'écoulement noyé en dessous à ressaut éloigné ou à ressaut recouvrant le pied de nappe qui provoque une augmentation du débit par rapport au régime dénoyé.
Ces aspects pourraient être explorés en consolidant le résultat par de nouveaux
jeux de données sur des angles d'ouverture du triangle différentes des deux
ouvertures testées.
