# Fairify configuration: don't change the following lines
fairify::render_report_setup(this.path::this.dir())

# Add below specific configuration for this report
knitr::opts_chunk$set(fig.path="figures/",
                      out.width = "70%")

# Load this package and its configuration
library(tidyverse)
library(ggrepel)
library(CassiopeeR)
pkgload::load_all()

# Load configuration
input.path <- file.path(pkgload::pkg_path(), "input")
cache.path <- file.path(pkgload::pkg_path(), "cache")
output.path <- file.path(pkgload::pkg_path(), "output")
cfg <- config::get(file = file.path(input.path, "config.yml"))

getFigWidth <- function(out.width = knitr::opts_chunk$get("out.width")) {
  6 * readr::parse_number(out.width) /  70
}
knitr::opts_chunk$set(
  fig.width = getFigWidth()
)
isKnit <- isTRUE(getOption('knitr.in.progress'))
