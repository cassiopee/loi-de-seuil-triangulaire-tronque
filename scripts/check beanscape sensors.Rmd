---
title: "Check BeanScape sensors"
output: html_notebook
---

```{r setup}
path <- "../input/BeanScape"
sensorSets <- list.files(path = path, pattern = "^Folder .*$")
data_files <- sapply(sensorSets, function(folder) {
  dirs <- list.dirs()
})
```

