----------------------------------------------------------------------------------------------
BeanDevice : AN V
MAC_ID : 00158D00000E0AE7
PAN_ID : 0EC5
Network Id : 0001
DATE_FORMAT : dd/MM/yyyy HH:mm:ss
Date : 25/07/2023 15:06:11
Data acquisition cycle : 12
Ratio : 1|1|1|1
Offset : 0|0|0|0

Unit : V
----------------------------------------------------------------------------------------------
 
Date;Measures Ch_V_0(V);Ch_V_1(V);Ch_V_2(V);Ch_V_3(V)
 
25/07/2023 15:06:09;0,1171;0,1162;0,1059;0,1022
